import subprocess
from math import *
from random import *

#Internamente se representara el laberinto como lista de listas, que inician en la posicion (0,0)
#y final en (n-1,n-1)
#aunque la entrada y la salida seran de la forma (1,1) a (n,n).

#importar_tablero: string -> lista(lista(int)), tupla, tupla
#importa y parcea el tablero. Genera una matriz en la cual cada elemento es una lista.
#el primer elemento de esta lista es un char:
#	*)-1 si es pares
#	*)-2 si es un espacio por donde caminar
#	*)-4 si es la posicion final
#	*)-3 si es la posicion inicial

def importar_tablero(nombre_archivo = 'Output.txt'):
    archivo = open(nombre_archivo,'r+')
    tablero = []
    inicio = (0,0)
    final = (0,0)
    for i,l in enumerate(archivo):
        #print(i)
        l = list(l)
        l.pop()
        for j,_ in enumerate(l):
            if l[j] == 'I':
                inicio = (i,j)
                print(inicio)
            elif l[j] == 'X':
                final = (i,j)
                print(final)
            l[j] = caracter_numero(l[j])
        tablero.append(l)
    archivo.close()
    print("carga Terminada")
    return tablero,inicio,final


###############################################
#funciones de posicion:

#int_punto: numb -> tupla(x,y)
#Dada una posicion de forma unidimencional la devuelve de forma
#bidimencional
def int_punto(posicion, dimension):
	return(floor(posicion/dimension), posicion % dimension)

#punto_int:tupla(x,y) -> numb
#Dado un punto de forma bidimencional la devuelve de forma unidimencional.
def punto_int(posicion, dimension):
	return(dimension * posicion[0] + posicion[1])

#caracter_numero: str -> numb
#Dado un string de los que representan puntos de interes en el tablero
#devuelve un numero que se asociara a este punto de interes
def caracter_numero(caracter):
    ralacion = {"1":-1,"0":-2,"I":-3,"X":-4}
    return ralacion[caracter]

##############################################    

#encontar_camino: list(lis()), tuppla, tupla -> boolean
#Esta funcion dado un laberinto, su posicion inicial y final
#encuentra el (si es que exste) un camino entre ambos puntos.
#Es una inplementacion de BFS.
def encontar_camino(laberinto, inicio, final): 
	print("buscando camino")
	#inicio -> (x,y)
	#final  -> (x,y)
	#actual -> (x,y)
	queue = [inicio] 
	actual = inicio	
	contador = 0
	dimencion = len(laberinto)
	while queue and (actual != final):
		pos_x = actual[0]
		pos_y = actual[1]
		#if not fueVisitado(laberinto[pos_x][pos_y]):
		queue += vecinos(actual,laberinto,dimencion)
		queue.pop(0)
		if queue:
			actual = queue[0]
	if(actual == final):
		print("Camino encontrado")
		return True
	else:
		print("Camino NO encontrado")
		return False

#verificar_dir: tupla(x, y), int -> list()
#Esta funcion nos debuelve los vecinos del punto actual, solo si no son
#estaban visitados y estan dento del laberinto.   
def verificar_dir(actual, dimencion):
	verificados = []
	pos_x = actual[0]
	pos_y = actual[1]
	for i in ((1,0),(-1,0),(0,1),(0,-1)):
		if ((0 <= pos_x + i[0]) and (0 <= pos_y + i[1])) and ((dimencion > pos_x + i[0]) and (dimencion > pos_y + i[1])):
			verificados.append((pos_x + i[0], pos_y + i[1]))
	return verificados

########################################################
#funciones Booleanas:

#NoesPared: tupla(x,y), list(list()) -> Boolean
#Esta funcion verifica que un punto del laberinto no es un tablero.
def NoesPared(punto, laberinto):
    return laberinto[punto[0]][punto[1]] != -1

#fueReferenciado: tupla, list(list()) -> Boolean
#Esta funcion debuelve true si una posicion del tablero no es vacia ni la llegada.
def fueReferenciado(punto, laberinto):
	casilla = laberinto[punto[0]][punto[1]]
	return -2 != casilla and casilla != -4
#######################################################


#vecinos: tupla, list(list()), number -> list   
#dado un punto, verifica que este no se encuntre visitado, y ademas da sus vecinos. agregando que fueron referenciados
#por este punto
def vecinos(actual, laberinto, dimencion):
	#actual -> (x,y)
	vecinos = []
	puntos = verificar_dir(actual, dimencion) 
	for i in puntos:
		#i -> (x,y)
		if NoesPared(i, laberinto) and not fueReferenciado(i, laberinto):
			vecinos.append(i)
			laberinto[i[0]][i[1]] = punto_int(actual,dimencion)#deTupla_Tupla(laberinto[i[0]][i[1]],actual)
	return vecinos
	
#camino: list(list()), tupla(), tupla() -> lista()
#Esta funcion es la encargada de recuperar el camino recorrido desde el final al inicio del laberinto. 
def camino (tablero, inicial, final):
	print("construyendo el camino")
	camino = [final]
	actual = final
	dimencion = len(tablero)
	while actual != inicial:
		pos_x = actual[0]
		pos_y = actual[1]
		actual = int_punto(tablero[pos_x][pos_y],dimencion)
		camino.append(actual)
	return camino

#salida: list(tuplas), string ->
#Dado un camino, lo imprime en un archivo de texto. 
def salida(camino, nombre_salida = "SalidaPY.txt"):
	print("imprimiendo salida")
	archivo = open(nombre_salida,"w+")
	i = len(camino) - 1
	while i >= 0:
		actual = camino[i]
		temp1 = str(actual[0] + 1)
		temp2 = str(actual[1] + 1)
		if i > 0:
			archivo.write('(' + temp1 +','+ temp2 + '),')
		else:
			archivo.write('(' + temp1 +','+ temp2 + ')\n')
		i-=1
	archivo.close()
	
#resolver_laberinto: string, string, string, string ->
#Llamando a esta funcion se genera un laberinto y se intenta resolver.
#Se generaran laverintos hasta que se genere uno con solucion.
#ADVERTENCIA: esta funcion supone que existe la posivilidad de desplazarce entre los puntos de alguna forma.
def resolver_laberinto(archivoC_compilado = "a.out", entradaC = "EntradaLaberinto.txt", salidaC = "laberinto.txt", solucion = "solucion.txt"):
	archivoC_compilado = "./" + archivoC_compilado
	b = False
	ERROR = False
	while not b and not ERROR:
		seed = str(floor(random()*10000000000))
		response = subprocess.run([archivoC_compilado, entradaC, salidaC, seed]) #llamada al programa de .c
		if response.returncode == 0: #en caso de no existir errores registrados por el programa de c.
			partida = importar_tablero(salidaC)
			b = encontar_camino(partida[0], partida[1], partida[2])
		else:
			ERROR = True
	if not ERROR:
		salida(camino (partida[0], partida[1], partida[2]))
	else:
		print("Se registran errores")
  
resolver_laberinto()
