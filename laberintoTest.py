from Laberintos import *
import subprocess

def test_encontar_camino():
	mano = importar_tablero("test01.txt")
	assert (encontar_camino(mano[1], mano[1], mano[1]))


def test_c ():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	llamada_c = ["./a.out", "laberinto.txt", "1"]
	for i in casos_test[0]:
		print(i)
		response = subprocess.run([llamada_c[0], casos_test[0][1], llamada_c[1], llamada_c[2]])
		temp = response.returncode
		assert temp == 1
	archivo.close()

def test_importar_tablero():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	assert importar_tablero("lab_test01.txt") == casos_test[1]
	archivo.close()

def test_int_punto():
	assert int_punto(10,4) == (2,2)
	assert int_punto(15,4) == (3,3)
	assert int_punto(2,4) == (0,2)
	assert int_punto(0,4) == (0,0)
	
def test_punto_int():
	assert punto_int((2,2),4) == 10 
	assert punto_int((0,2),4) == 2 
	assert punto_int((3,3),4) == 15 
	assert punto_int((0,0),4) == 0 
	
def test_caracter_numero():
	assert caracter_numero('1') == -1
	assert caracter_numero('0') == -2
	assert caracter_numero('X') == -4
	assert caracter_numero('I') == -3
	
def test_verificar_dir():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	for i in casos_test[2]:
		assert verificar_dir(i[0],4) == i[1]
	archivo.close()
		
def test_encontar_camino():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	for i in casos_test[3]:
		assert encontar_camino(i[0],i[1],i[2])
	archivo.close()


def test_vecinos():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	i = casos_test[4]
	assert vecinos(i[0], i[1], i[2]) == i[3]
	assert  i[1] == i[4]
	archivo.close()

def test_camino():
	archivo = open('pruebas.txt', 'r')
	casos_test = eval(archivo.read())
	i = casos_test[5]
	assert camino(i[0],i[1],i[2]) == i[3]
	archivo.close()
