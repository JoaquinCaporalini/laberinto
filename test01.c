#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

char ** matrix(long cant_filas, long cant_columnas){
	char ** mtrx;
	
	mtrx = malloc(sizeof(char *) * cant_filas);
	
	for (long i = 0; i < cant_filas; i++){
		mtrx[i] = malloc(sizeof(char) * (cant_columnas + 1));
		for(long j = 0; j < cant_columnas; j++){
			mtrx[i][j] = '0';
			}
		mtrx[i][cant_columnas] = '\0';
		}
	return mtrx;	
	}


int main(){
	char **tablero;
	int a = 100, b = 100;
	
	tablero = matrix(a, b);
	for(int i = 0; i < a; i++){printf("%s\n",tablero[i]);}
	
	return 0;
	}
