def archivo_open(nombre_archivo = 'ejemplo.txt'):
    archivo = open(nombre_archivo,'r+')
    tablero = archivo.readlines()
    archivo.close()
    cant_lineas = len(tablero)
    long_linea = len(tablero[0])
    for i in range(cant_lineas):
        tablero[i] = list(tablero[i])
        tablero[i][long_linea-1] = '1'
        #print(tablero[i])
    tablero.append(list('1'*long_linea))
    #print(tablero[cant_lineas])
    return tablero

def tablerator(dimencion = 5, nombre_archivo = 'ejemplo.txt'):
    archivo = open(nombre_archivo,'w+')
    salida = ('0'*dimencion)+'\n'
    print(salida)
    for i in range(dimencion):
        archivo.write(salida)

def grafo(matriz):
    cant_filas = len(matriz)-1
    cant_columnas = len(matriz[0])-1
    nodos = {}
    for i in range(cant_filas):
        for j in range(cant_filas):
            if matriz[i][j] != '1':
                temp = []
                for k in (1,-1):
                    if matriz[i+k][j] != '1':
                        temp.append((i+k,j))
                for k in (1,-1):
                    if matriz[i][j+k] != '1':
                        temp.append((i,j+k))
                nodos[(i,j)] = temp
    return nodos

#tablerator(1000)
#grafo(archivo_open())

def bfs(graph, start, goal):
    if start == goal:
        print([start])
        return
    
    queue = [start]

    # dict which holds parents, later helpful to retreive path.
    # Also useful to keep track of visited node
    parent = {}
    parent[start] = start
    b = True
    while queue and b:
        currNode = queue.pop(0)
        for neighbor in graph[currNode]:
            # goal found
            if neighbor == goal:
                parent[neighbor] = currNode
                print_path(parent, neighbor, start)
                b = False
            # check if neighbor already seen
            if neighbor not in parent:
                parent[neighbor] = currNode
                queue.append(neighbor)
    if b:
        print("No path found.")


def print_path(parent, goal, start):
    path = [goal]
    # trace the path back till we reach start
    while goal != start:
        goal = parent[goal]
        path.append(goal)
    path.reverse()
    print(path)
#hay que hacer reverse?

def resolver():
    graph = grafo(archivo_open())
    bfs(graph, (0,0), (2,2))
