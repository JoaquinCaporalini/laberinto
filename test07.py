import subprocess
from random import *

def importar_tablero(nombre_archivo = 'Output.txt'):
	archivo = open(nombre_archivo,'r+')
	tablero = archivo.readlines()
	archivo.close()
	dimencion = len(tablero)
	inicio = (0,0)
	final = (0,0)
	for i in range(dimencion):
		tablero[i] = list(tablero[i])
		tablero[i].pop()
		for j in range(dimencion):
			if tablero[i][j] == 'I':
				inicio = (i,j)
				print(inicio)
			elif tablero[i][j] == 'X':
				final = (i,j)
				print(final)
			tablero[i][j] = [tablero[i][j],(-1,-1),False]
	return tablero,inicio,final

def camino (tablero, inicial, final):
	camino = [final]
	actual = final
	while actual != inicial:
		print(actual)
		pos_x = actual[0]
		pos_y = actual[1]
		actual = tablero[pos_x][pos_y][1]
		camino.append(actual)
	camino.reverse()
	return camino
		
def bfs(mtx, I, X): #I, X son tuplas(x,y)					#
	queue = [I]
	actual = I
	contador = 0
	dimencion = len(mtx)
	while queue and (actual != X):
		pos_x = actual[0]
		pos_y = actual[1]
		if not mtx[pos_x][pos_y][2]:
			queue += vecinos(actual,mtx,dimencion)
			mtx[pos_x][pos_y][2] = True
		queue.pop(0)
		if queue:
			actual = queue[0]
	if(actual == X):
		print("nice")
		return True
	else:
		print("no nice")
		return False
    
def verificar_dir(actual, dimencion):
	verificados = []
	pos_x = actual[0]
	pos_y = actual[1]
	for i in ((1,0),(-1,0),(0,1),(0,-1)):
		if ((0 <= pos_x + i[0]) and (0 <= pos_y + i[1])) and ((dimencion > pos_x + i[0]) and (dimencion > pos_y + i[1])):
			verificados.append((pos_x + i[0], pos_y + i[1]))
	return verificados
	    
def vecinos(actual, mtx, dimencion):
	vecinos = []
	puntos = verificar_dir(actual, dimencion) 
	for i in puntos:
		if mtx[i[0]][i[1]][0] != '1':
			vecinos.append(i)
			if mtx[i[0]][i[1]][1] == (-1,-1):
				mtx[i[0]][i[1]][1] = actual
	return vecinos
		  
def salida(camino, nombre_salida = "SalidaPY.txt"):
	archivo = open(nombre_salida,"w+")
	for i in camino:
		temp1 = str(i[0] + 1)
		temp2 = str(i[1] + 1)
		archivo.write('(' + temp1 +','+ temp2 + ')\n')
	archivo.close()
	
  
def dummy(archivoC_compilado = "a.out", entradaC = "EntradaLaberinto.txt", salidaC = "laberinto.txt", solucion = "solucion.txt"):
	archivoC_compilado = "./" + archivoC_compilado
	b = False
	ERROR = False
	while not b and not ERROR:
		seed = str(int(random()*10000000000))
		response = subprocess.run([archivoC_compilado, entradaC, salidaC, seed])
		if response.returncode == 0:
			partida = importar_tablero(salidaC)
			b = bfs(partida[0], partida[1], partida[2])
		else:
			ERROR = True
	if not ERROR:
		salida(camino (partida[0], partida[1], partida[2]))
	else:
		print("Se registran errores")
  
dummy('test08', 'hola.txt')
