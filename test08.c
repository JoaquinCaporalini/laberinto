#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <math.h>


//32768

enum {
	Dimencion, Obstaculos_fijos, Obstaculos_aleatorios, Posicion_inicial, Objetivo
	};

//~ void intcopy(int *hasta, int *desde, int cant_elem){
	//~ for (int i = 0; i <= cant_elem;i++){hasta[i] = desde[i];}
	//~ }

int verificar(char * linea){
	int i = -1;
	if (!strcmp(linea,"dimencion")){i = Dimencion;}
		else{ 
			if (!strcmp(linea,"obstaculos fijos")){i = Obstaculos_fijos;}
			else{ 
				if (!strcmp(linea,"obstaculos aleatorios")){i = Obstaculos_aleatorios;}
				else{ 
					if (!strcmp(linea,"posicion inicial")){i = Posicion_inicial;}
					else{ 
						if (!strcmp(linea,"objetivo")){i = Objetivo;}
						}}}}
	return i;
	}
	
void datos(FILE * archivo,int *dimencion,long *cant_randoms,int *cant_fijos, int * ERROR){
	//printf("datos");
	char bufer[50];
	int cant_titulos = 0;
	for (int titulo = 0, temp; !feof(archivo);){
		fscanf(archivo,"%[^\n]\n",bufer);
		temp = verificar(bufer);
		if (temp != -1){
			titulo = temp;
			cant_titulos++;
			}else{
			switch(titulo){
				case(Dimencion):
					*dimencion = atoi(bufer);
					break;
				case(Obstaculos_fijos):
					*cant_fijos = *cant_fijos + 1;
					break;
				case(Obstaculos_aleatorios):
					*cant_randoms = atoi(bufer);
					break;
				default:
					temp = 0;
			}
		}}
	if(cant_titulos == 5){*ERROR = 1;}
	}

void leer_colocar(FILE *archivo, char **tablero, char fondo, int dimencion, int * ERROR){
	char bufer[100];
	char num1[25], num2[25];
	int  cant_titulos = 0;
	for (int titulo = 0, temp; !feof(archivo);){
		fscanf(archivo,"%[^\n]\n",bufer);
		temp = verificar(bufer);
		if (temp != -1){
			cant_titulos++;
			titulo = temp;
			}else{
			int x,y;
			switch(titulo){
				case(Objetivo):
					//printf("Objetivo/n");
					sscanf(bufer,"(%[^,],%[^)])",num1,num2);
					x = atoi(num1) -1;
					y = atoi(num2) -1;
					if ((x >= 0) && (y >= 0) && (x < dimencion) && (y < dimencion)){
						if (tablero[x][y] == fondo){tablero[x][y] = 'X';}
						else{*ERROR = 1;}}
					else{*ERROR = 1;}
					break;
				case(Posicion_inicial):
					//printf("Posicion_inicial/n");
					sscanf(bufer,"(%[^,],%[^)])",num1,num2);
					x = atoi(num1) -1;
					y = atoi(num2) -1;
					if ((x >= 0) && (y >= 0) && (x < dimencion) && (y < dimencion)){
						if (tablero[x][y] == fondo){tablero[x][y] = 'I';}
						else{*ERROR = 1;}}
					else{*ERROR = 1;}
					break;
				case(Obstaculos_fijos):
					//printf("Obstaculos_fijos/n");
					sscanf(bufer,"(%[^,],%[^)])",num1,num2);
					x = atoi(num1) -1;
					y = atoi(num2) -1;
					if ((x >= 0) && (y >= 0) && (x < dimencion) && (y < dimencion)){
						if (tablero[x][y] == fondo){tablero[x][y] = '3';}
						else{*ERROR = 1;}}
					else{*ERROR = 1;}
					break;
				default:
					temp = 0;
			}
		}}
	if(cant_titulos == 5){*ERROR = 1;}
	}
	
	
char ** matrix(int cant_filas, int cant_columnas, char base){
	char ** mtrx;
	
	mtrx = malloc(sizeof(char *) * cant_filas);
	
	for (int i = 0; i < cant_filas; i++){
		mtrx[i] = malloc(sizeof(char) * (cant_columnas + 1));
		memset(mtrx[i], base, sizeof(char)* cant_columnas);
			
		mtrx[i][cant_columnas] = '\0';
		}
	return mtrx;	
	}

//verificar, esta funcion PUEDE no terminas
void randndomLaberinto(char **tablero, long cant_rand_pont, int size_mtrx, long semilla, char objetivo, char remplazo){
	int pos_x,pos_y;
	srand(semilla);
	
	for (long i = 0; i < cant_rand_pont;){
		pos_x = rand() % size_mtrx;
		pos_y = rand() % size_mtrx;
		if (tablero[pos_x][pos_y] == objetivo)
			{tablero[pos_x][pos_y] = remplazo;i++;}
		} 
	}

void desreforza_paredes(char ** tablero, int dimencion){
	for(int i = 0; i < dimencion; i++){
		for(int j = 0; j < dimencion;j++){
			if(tablero[i][j] == '3'){tablero[i][j] = '1';}
			}
		}
	}

//cambia -> pasar el archivo abierto
void salida(FILE * salidaTXT, char **tablero, int cant_lineas){
	for (int i = 0; i < cant_lineas; i++){
		fprintf(salidaTXT,"%s\n",tablero[i]);
		}
	}
//./test08 hola.txt salida.tx 1
// resive nombre_entrada nombre_salida semilla
int main(int argc, char * argv[]){
	if (argc <= 3){
		printf("Falatan argumentos\n");
		return 1;
		}
	
	FILE * archivo = fopen(argv[1], "r");
	if (archivo == NULL){printf("Error archivo de entrada");return 1;}
	
	int dimencion = 0, cant_fijos = 0, ERROR = 0;
	long cant_randoms = 0;
	char ** tablero;
	long semilla = atoi(argv[3]);
	
	datos(archivo, &dimencion, &cant_randoms, &cant_fijos, &ERROR); 
	
	if (ERROR == 0){
		if (cant_randoms <= (pow(dimencion,2) - cant_fijos - 2)){
			
			int estado;
			
			rewind(archivo);
			
			if ((pow(dimencion, 2)-2-cant_fijos)/2 > cant_randoms){
				tablero = matrix(dimencion, dimencion, '0');
				estado = 0;
				}else{
					tablero = matrix(dimencion, dimencion, '1');
					estado = 1;
					}
			
			if (estado == 0){
				leer_colocar(archivo, tablero, '0', dimencion, &ERROR); //falta
				}else{
					leer_colocar(archivo, tablero, '1', dimencion, &ERROR);
				}
				
			if (ERROR == 0){
				if (estado == 0){
					randndomLaberinto(tablero, cant_randoms, dimencion, semilla, '0', '1');
				}else{
					randndomLaberinto(tablero, cant_randoms, dimencion, semilla, '1', '0');
					}
				desreforza_paredes(tablero,dimencion); //falta
				
				FILE * salidaTXT = fopen(argv[2],"w+");
				if (salidaTXT != NULL){
					salida(salidaTXT,tablero,dimencion);
					fclose(salidaTXT);
				}else{printf("Problemas con el archivo de salida");}
				
			}else{printf("Problema con los puntos dados\n  *)puntos fuera deltablero\n  *)Puntos repetidos\n");}
			
			for(int i = 0; i < dimencion; i++){free(tablero[i]);}
			free(tablero);
			
		}else{printf("cantidad de randoms supera espacios libes\n");}
	}else{printf("Problemas con los titulos\n");}
	return ERROR;
	}
